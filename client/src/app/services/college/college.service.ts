import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { College } from 'src/app/shared/models/College.model';
import { Job } from 'src/app/shared/models/Job.model';
import { ServerConfig } from 'src/app/shared/server-config';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',

  })
}

@Injectable({
  providedIn: 'root',
})

export class CollegeService {
  private apiUrl = "http://localhost:3000/colleges";

  constructor(private http: HttpClient) {}

  public getCollegesByName(college: College): Observable<College> {
    return this.http.get<College>(`${this.apiUrl}/${college.collegeName}`);
  }

  // public getCollegeById(collegeId: College["collegeId"]): Observable<College> {
  //   return this.http.get<College>(
  //     `${ServerConfig.serverBaseUrl}${ServerConfig.collegeEndpoint}/${collegeId}`,
  //     httpOptions
  //   );
  // }

  public getCollegeById(collegeId: College["collegeId"]): Observable<College> {
    return this.http.get<College>(`${this.apiUrl}/${collegeId}`);
  }

  public getAllColleges(): Observable<College[]> {
    return this.http.get<College[]>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.collegeEndpoint}`,
      httpOptions
    );;
  }

  // public getCollegesByLocation(college: College): Observable<College> {
  //   return this.http.get<College>(`${this.apiUrl}/${college.collegeLocation}`);
  // }

  // public getCollegeByNirfRanking(college: College): Observable<College> {
  //   return this.http.get<College>(`${this.apiUrl}/${college.collegeNirfRanking}`);
  // }

  // public getCollegesSortedByNirfRanking(college: College): Observable<College> {
  //   return this.http.get<College>(this.apiUrl);
  // }

  // public getCollegesByAicteAffiliation(college: College): Observable<College> {
  //   return this.http.get<College>(`${this.apiUrl}/${college.collegeAicteAffiliation}`);
  // }

  // public getAllCollegesCustom(college: College): Observable<College> {
  //   return this.http.get<College>(this.apiUrl);
  // }

  // public getAllCollegeUsers(college: College): Observable<College> {
  //   return this.http.get<College>(this.apiUrl);
  // }


  public registerCollege(college: College): Observable<College> {
    return this.http.post<College>(this.apiUrl, college);
  }

  public updateCollege(college: College): Observable<College> {
    const url = `${this.apiUrl}/${college.collegeEmail}`;
    return this.http.put<College>(url, college);
  }

  public deleteCollege(college: College): Observable<College> {
    const url = `${this.apiUrl}/${college.collegeId}`;
    return this.http.delete<College>(url);
  }
}
