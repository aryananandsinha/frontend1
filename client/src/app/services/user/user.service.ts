import { Injectable, OnInit } from '@angular/core';
import { User } from 'src/app/shared/models/User.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { ServerConfig } from 'src/app/shared/server-config';
import { Company } from 'src/app/shared/models/Company.model';
import { College } from 'src/app/shared/models/College.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class UserService {
  loggedInUser!: User;
  allCollegeUsers: User[] = [];
  allCompanyUsers: User[] = [];

  constructor(private http: HttpClient) {}

  setLoggedInUser(loggedInuUser: User): void {
    this.loggedInUser = loggedInuUser;
  }

  setAllCollegeUsers(allCollegeUsers: User[]): void {
    this.allCollegeUsers = allCollegeUsers;
  }
  setAllCompanyUsers(allCompanyUsers: User[]): void {
    this.allCompanyUsers = allCompanyUsers;
  }

  loginUser(enteredUserDetails: any): Observable<User> {
    return this.http.post<User>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.loginAuthEndpoint}`,
      enteredUserDetails,
      httpOptions
    );
  }

  logoutUser(): Observable<any> {
    return this.http.post<any>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.logoutAuthEndpoint}`,
      httpOptions
    );
  }

  registerUser(enteredUserDetails: any): Observable<User> {
    return this.http.post<User>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.registerAuthEndpoint}`,
      enteredUserDetails,
      httpOptions
    );
  }

  getUserById(userId: Number): Observable<User> {
    return this.http.get<User>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.userEndpoint}/${userId}`,
      httpOptions
    );
  }

  deleteUserById(userId: Number): Observable<User> {
    return this.http.delete<any>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.userEndpoint}/${userId}`,
      httpOptions
    );
  }

  updateUser(user: User): Observable<User> {
    const userId = user.userAccountId;
    return this.http.put<User>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.userEndpoint}/${userId}`,
      user,
      httpOptions
    );
  }

  getAllCollegeUsers(): Observable<User[]> {
    const collegeId = this.loggedInUser.userCollege?.collegeId;
    return this.http.get<User[]>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.collegeUsersAdminEndpoint}/${collegeId}`,
      httpOptions
    );
  }

  getAllCompanyUsers(): Observable<User[]> {
    const companyId = this.loggedInUser.userCompany?.companyId;
    return this.http.get<User[]>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.companyUsersAdminEndpoint}/${companyId}`,
      httpOptions
    );
  }

  approveCompanySpoc(userId: number): Observable<User> {
    return this.http.post<User>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.approveCompanySpocAdminEndpoint}/${userId}`,
      httpOptions
    );
  }

  approveCollegeSpoc(userId: number): Observable<User> {
    return this.http.post<User>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.approveCollegeSpocAdminEndpoint}/${userId}`,
      httpOptions
    );
  }

  applyToCompany(company: Company): Observable<any> {
    return this.http.put<any>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.applyCompanyEndpoint}`,
      company,
      httpOptions
    );
  }

  applyToCollege(college: College): Observable<any> {
    return this.http.put<any>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.applyCollegeEndpoint}`,
      college,
      httpOptions
    );
  }
}
