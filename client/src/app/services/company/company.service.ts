import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Company } from 'src/app/shared/models/Company.model';
import { Observable } from 'rxjs';
import { ServerConfig } from 'src/app/shared/server-config';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root'
})

export class CompanyService {

  constructor(private httpClient: HttpClient) { }

  // Get all companies
  getAllCompanies(): Observable<Company[]> {
    return this.httpClient.get<Company[]>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.companyEndpoint}`,
      httpOptions
    );
  }

  // Get company by Id
  getCompanyById(companyId: Number): Observable<Company> {
    return this.httpClient.get<Company>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.companyEndpoint}/${companyId}`,
      httpOptions
    );
  }

  // // Post company
  registerCompany(enteredCompanyDetails: any): Observable<Company> {
    return this.httpClient.post<Company>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.companyEndpoint}`,
      enteredCompanyDetails,
      httpOptions
    );
  }

  // // Update company
  updateCompany(company: Company): Observable<Company> {
    const companyId = company.companyId;
    return this.httpClient.post<Company>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.companyEndpoint}/${companyId}`,
      company,
      httpOptions
    );
  }

  // // Delete company by Id
  deleteCompanyById(companyId: Number): Observable<any> {
    return this.httpClient.delete<any>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.companyEndpoint}/${companyId}`,
      httpOptions
    );
  }
}
