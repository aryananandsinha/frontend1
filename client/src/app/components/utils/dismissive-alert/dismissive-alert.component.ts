import { Component, OnInit } from '@angular/core';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';

@Component({
  selector: 'app-dismissive-alert',
  templateUrl: './dismissive-alert.component.html',
  styleUrls: ['./dismissive-alert.component.css'],
})
export class DismissiveAlertComponent implements OnInit {
  alerts: any[] = [];

  add(type: string, msg: string, timeout: number): void {
    this.alerts.push({
      type: type,
      msg: msg,
      timeout: timeout,
    });
    console.log(this.alerts);
  }

  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter((alert) => alert !== dismissedAlert);
  }

  constructor() {}

  ngOnInit(): void {}
}
