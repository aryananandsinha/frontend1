import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { UserType } from 'src/app/shared/enums/UserType-enum.model';
import { User } from 'src/app/shared/models/User.model';
import { UserName } from 'src/app/shared/models/UserName.model';

@Component({
  selector: 'app-user-detail-form',
  templateUrl: './user-detail-form.component.html',
  styleUrls: ['./user-detail-form.component.css'],
})
export class UserDetailFormComponent implements OnInit {
  @Input()
  user!: User;

  @Output()
  formSubmit: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  onFormSubmit(): void {
    this.formSubmit.emit(this.user);
  }
}
