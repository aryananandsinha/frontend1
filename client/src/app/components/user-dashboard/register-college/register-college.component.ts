import { Component, OnInit } from '@angular/core';
import { CollegeService } from 'src/app/services/college/college.service';
import { College } from 'src/app/shared/models/College.model';

@Component({
  selector: 'app-register-college',
  templateUrl: './register-college.component.html',
  styleUrls: ['./register-college.component.css'],
})
export class RegisterCollegeComponent implements OnInit {
  college: College = {
    collegeId: 12323,
    collegeName: '',
    collegeWebsite: '',
    collegeNirfRanking: 0,
    collegeAicteAffiliation: false,
    collegeLocation: {
      locationCity: '',
      locationCountry: '',
      locationDistrict:'',
      locationPincode:'',
      locationState:''
    },
    collegeEmail: '',
  };
  constructor(private collegeService: CollegeService) {}

  ngOnInit(): void {}
}
