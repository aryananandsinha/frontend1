import { Component, OnInit } from '@angular/core';
import { CompanyService } from 'src/app/services/company/company.service';
import { UserService } from 'src/app/services/user/user.service';
import { Company } from 'src/app/shared/models/Company.model';

@Component({
  selector: 'app-search-company',
  templateUrl: './search-company.component.html',
  styleUrls: ['./search-company.component.css']
})
export class SearchCompanyComponent implements OnInit {

  companies: Company[] = [
    {companyName: 'PeopleStrong',
    companyDescription: 'We power the momentum of modern talent-focused enterprises. We power the experience of your talent workforce, help you take data-driven decisions that impact your people, and bring agility to your business operations',
    companyWebsite: 'https://www.peoplestrong.com/',
    companyCin: 'L21091KA2019OPC141331',
    companyImage: ''},
    {companyName: 'PeopleStrong-1',
    companyDescription: 'We power the momentum of modern talent-focused enterprises. We power the experience of your talent workforce, help you take data-driven decisions that impact your people, and bring agility to your business operations',
    companyWebsite: 'https://www.peoplestrong.com/',
    companyCin: 'L21091KA2019OPC141331',
    companyImage: ''},
    {companyName: 'PeopleStrong-2',
    companyDescription: 'We power the momentum of modern talent-focused enterprises. We power the experience of your talent workforce, help you take data-driven decisions that impact your people, and bring agility to your business operations',
    companyWebsite: 'https://www.peoplestrong.com/',
    companyCin: 'L21091KA2019OPC141331',
    companyImage: ''}
  ];

  public isCollapsed = -1;

  constructor(private companyService: CompanyService, private userService: UserService) { }

  ngOnInit(): void {
    // this.companyService.getAllCompanies().subscribe({
    //   next: (result) => {
    //     this.companies = result;
    //   },
    //   error: (err) => {
    //     console.log(err);
    //   }
    // })
  }

  userRequestCompany(company: Company) {
    // Apply for company - Don't know endpoint
    alert("Applied for " + company.companyName);
  }
}

