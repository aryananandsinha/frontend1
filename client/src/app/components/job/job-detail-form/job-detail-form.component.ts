import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-job-detail-form',
  templateUrl: './job-detail-form.component.html',
  styleUrls: ['./job-detail-form.component.css'],
})
export class JobDetailFormComponent implements OnInit {
  constructor() {}
  categories = [
    { id: 1, name: 'Haryana' },
    { id: 2, name: 'Bangalore' },
    { id: 3, name: 'Pune' },
    { id: 4, name: 'Mumbai' },
    { id: 5, name: 'Bhubaneswar' },
    { id: 6, name: 'Delhi' },
  ];
  selected: any;
  multiple: boolean;
  getSelectedValue() {
    console.log(this.selected);
  }
  categories_skills = [
    { id: 1, name: 'C' },
    { id: 2, name: 'C++' },
    { id: 3, name: 'Java' },
    { id: 4, name: 'Python' },
    { id: 5, name: 'DSA' },
    { id: 6, name: 'Spring Boot' },
  ];
  selected_skills: any;
  multiple_skills: boolean;
  getSelectedSkills() {
    console.log(this.selected_skills);
  }
  categories_qualifications = [
    { id: 1, name: 'B.tech' },
    { id: 2, name: 'M.tech' },
    { id: 3, name: 'BCA' },
    { id: 4, name: 'MCA' },
  ];
  selected_qualifications: any;
  multiple_qualifications: boolean;
  getSelectedQualifications() {
    console.log(this.selected_skills);
  }

  ngOnInit(): void {
    //this.selected=this.categories[0];
  }
}
