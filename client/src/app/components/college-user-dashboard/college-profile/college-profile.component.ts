import { Component, OnInit, ViewChild } from '@angular/core';
import { CollegeService } from 'src/app/services/college/college.service';
import { UserService } from 'src/app/services/user/user.service';
import { College } from 'src/app/shared/models/College.model';
import { User } from 'src/app/shared/models/User.model';
import { UserType } from 'src/app/shared/enums/UserType-enum.model';
import { BehaviorSubject, Subject } from 'rxjs';
import { DismissiveAlertComponent } from '../../utils/dismissive-alert/dismissive-alert.component';

@Component({
  selector: 'app-college-profile',
  templateUrl: './college-profile.component.html',
  styleUrls: ['./college-profile.component.css'],
})
export class CollegeProfileComponent implements OnInit {
  college!: College;
  disableSwitching: boolean=false;

  // Check for UserType
  user!: User;

  constructor(
    private collegService: CollegeService,
    private userService: UserService
  ) {}

  @ViewChild(DismissiveAlertComponent)
  alert!: DismissiveAlertComponent;

  ngOnInit(): void {
    // this.signedInUser.userType = UserType.COLLEGE_SPOC;
    // // this.signedInUser.userType = UserType.COLLEGE_ADMIN;
    // if (this.signedInUser.userType == this.role) {
    //   this.showButton = true;
    // } else {
    //   this.showButton = false;
    // }
    this.disableSwitching = true;
  }
}
