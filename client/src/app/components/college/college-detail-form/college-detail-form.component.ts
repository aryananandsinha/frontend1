import { Component, Input, OnInit } from '@angular/core';
import { College } from 'src/app/shared/models/College.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CollegeService } from 'src/app/services/college/college.service';

@Component({
  selector: 'app-college-detail-form',
  templateUrl: './college-detail-form.component.html',
  styleUrls: ['./college-detail-form.component.css'],
})
export class CollegeDetailFormComponent implements OnInit {
  
  @Input()
  college: College;
  collegeDetailsForm: FormGroup;
  // @Input() displayStatus: boolean = true;
  // @Input() loadComponent: boolean = true;

  constructor(private collegeService: CollegeService) {}

  ngOnInit() {
    this.collegeDetailsForm = new FormGroup({
      collegeName: new FormControl(null, Validators.required),
      collegeWebsite: new FormControl(null, Validators.required),
      collegeEmail: new FormControl(null, [
        Validators.required,
        Validators.email,
      ]),
      collegeAicteAffiliation: new FormControl(null, Validators.required),
      collegeNirfRanking: new FormControl(null, Validators.required),
      collegeLocationCity: new FormControl(null, Validators.required),
      collegeLocationDistrict: new FormControl(null, Validators.required),
      collegeLocationState: new FormControl(null, Validators.required),
      collegeLocationPincode: new FormControl(null, Validators.required),
      collegeLocationCountry: new FormControl(null, Validators.required),
    });
  }

  collegeFormSubmit() {
    if (
      this.collegeName?.invalid ||
      this.collegeWebsite?.invalid ||
      this.collegeEmail?.invalid ||
      this.collegeAicteAffiliation?.invalid ||
      this.collegeNirfRanking?.invalid ||
      this.collegeLocationCity?.invalid ||
      this.collegeLocationDistrict?.invalid ||
      this.collegeLocationState?.invalid ||
      this.collegeLocationPincode?.invalid ||
      this.collegeLocationCountry?.invalid
    ) {
      alert('Empty fields not allowed!');
      return;
    } else {
      this.college = this.collegeDetailsForm.value;
      this.collegeService.registerCollege(this.college).subscribe(
        (data) => {
          console.log(data);
          alert('Details Updated Successfully!');
          this.collegeDetailsForm.reset();
        },
        (error) => {
          console.log(error);
          alert('Something Went Wrong!');
        }
      );
      // console.log(this.college);
      return;
    }
  }

  get collegeName() {
    return this.collegeDetailsForm.get('collegeName');
  }
  get collegeWebsite() {
    return this.collegeDetailsForm.get('collegeWebsite');
  }
  get collegeEmail() {
    return this.collegeDetailsForm.get('collegeEmail');
  }
  get collegeAicteAffiliation() {
    return this.collegeDetailsForm.get('collegeAicteAffiliation');
  }

  get collegeNirfRanking() {
    return this.collegeDetailsForm.get('collegeNirfRanking');
  }
  get collegeLocationCity() {
    return this.collegeDetailsForm.get('collegeLocationCity');
  }
  get collegeLocationDistrict() {
    return this.collegeDetailsForm.get('collegeLocationDistrict');
  }
  get collegeLocationState() {
    return this.collegeDetailsForm.get('collegeLocationState');
  }
  get collegeLocationPincode() {
    return this.collegeDetailsForm.get('collegeLocationPincode');
  }
  get collegeLocationCountry() {
    return this.collegeDetailsForm.get('collegeLocationCountry');
  }
}

