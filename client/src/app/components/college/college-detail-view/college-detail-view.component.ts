import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { College } from 'src/app/shared/models/College.model';
import { CollegeService } from 'src/app/services/college/college.service';

@Component({
  selector: 'app-college-detail-view',
  templateUrl: './college-detail-view.component.html',
  styleUrls: ['./college-detail-view.component.css'],
})
export class CollegeDetailViewComponent implements OnInit {
  
  @Input()
  college: College;

  constructor(private collegeService: CollegeService) {}

  ngOnInit(): void {
    // take collegeId dynamically from the loggedIn User
    this.getCollegeDetails(2).subscribe((College) => (this.college = College));
    // this.updateProfile.emit();
  }

  getCollegeDetails(collegeId: College['collegeId']) {
    return this.collegeService.getCollegeById(collegeId);
  }
}
