import { Component, Input, OnInit } from '@angular/core';
import { Company } from 'src/app/shared/models/Company.model';

@Component({
  selector: 'app-company-detail-view',
  templateUrl: './company-detail-view.component.html',
  styleUrls: ['./company-detail-view.component.css']
})
export class CompanyDetailViewComponent implements OnInit {

  @Input()
  company!: Company;

  constructor() { }

  ngOnInit(): void {
  }

}
