import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CompanyService } from 'src/app/services/company/company.service';
import { Company } from 'src/app/shared/models/Company.model';
import { DismissiveAlertComponent } from '../../utils/dismissive-alert/dismissive-alert.component';

@Component({
  selector: 'app-company-detail-form',
  templateUrl: './company-detail-form.component.html',
  styleUrls: ['./company-detail-form.component.css']
})
export class CompanyDetailFormComponent implements OnInit{

  company: Company;

  updateForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private companyService: CompanyService) { }

  @ViewChild(DismissiveAlertComponent)
  alert!: DismissiveAlertComponent;

  ngOnInit(): void {
    this.updateForm = this.formBuilder.group({
      id: null,
      inputName: [''],
      inputDesc: [''],
      inputWebsite: [''],
      inputCin: ['', ],
      inputImage: null
    });
  }

  onRegister(): void {
    // this.companyService.registerCompany(this.updateForm.value).subscribe({
    //   next: (result) => {
    //     this.company = result;
    //     this.alert.add('success', 'Company details registered sucessfully', 2000);
    //   },
    //   error: (err) => {
    //     this.alert.add('danger', 'Error', 3000);
    //   }
    // })

    this.company = this.updateForm.value;
    console.warn('Company updated successfully');
    console.log(this.company);
    
    this.updateForm.reset();
  }
}
