import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyJobsDetailComponent } from './company-jobs-detail.component';

describe('CompanyJobsDetailComponent', () => {
  let component: CompanyJobsDetailComponent;
  let fixture: ComponentFixture<CompanyJobsDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyJobsDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyJobsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
