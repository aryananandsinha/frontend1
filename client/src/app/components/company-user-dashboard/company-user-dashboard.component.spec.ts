import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyUserDashboardComponent } from './company-user-dashboard.component';

describe('CompanyUserDashboardComponent', () => {
  let component: CompanyUserDashboardComponent;
  let fixture: ComponentFixture<CompanyUserDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyUserDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyUserDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
