import { Component, OnInit, ViewChild } from '@angular/core';
import { CompanyService } from 'src/app/services/company/company.service';
import { UserType } from 'src/app/shared/enums/UserType-enum.model';
import { Company } from 'src/app/shared/models/Company.model';
import { User } from 'src/app/shared/models/User.model';
import { DismissiveAlertComponent } from '../../utils/dismissive-alert/dismissive-alert.component';

@Component({
  selector: 'app-company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.css']
})
export class CompanyProfileComponent implements OnInit {

  company: Company = {companyName: 'PeopleStrong',
                      companyDescription: 'We power the momentum of modern talent-focused enterprises. We power the experience of your talent workforce, help you take data-driven decisions that impact your people, and bring agility to your business operations',
                      companyWebsite: 'https://www.peoplestrong.com/',
                      companyCin: 'L21091KA2019OPC141331',
                      companyImage: ''};
  
  // formCompany: Company = {companyName: 'PeopleStrong',
  //                       companyDescription: 'We power the momentum of modern talent-focused enterprises. We power the experience of your talent workforce, help you take data-driven decisions that impact your people, and bring agility to your business operations',
  //                       companyWebsite: 'https://www.peoplestrong.com/',
  //                       companyCin: 'L21091KA2019OPC141331',
  //                       companyImage: ''};
  
  // Check for UserType                    
  user!: User;
  disableSwitching: boolean;

  constructor(private companyService: CompanyService) { }

  @ViewChild(DismissiveAlertComponent)
  alert!: DismissiveAlertComponent;

  ngOnInit(): void {
    // this.companyService.getCompanyById(companyId).subscribe({
    //   next: (result) => {
    //     this.company= result;
    //     //send a copy to user-detail-form
    //     this.formCompany = JSON.parse(JSON.stringify(this.company));
    //     this.alert.add('info', 'Company details fetched', 2000);
    //   },
    //   error: (err) => {
    //     this.alert.add('danger', 'Company details could not be fetched', 8000);
    //   },
    //   complete: () => console.info('complete'),
    // });

    // // Check if admin or not (By Default - false)
    // if (this.user.userType == UserType.COLLEGE_ADMIN) {
    //   this.disableSwitching = true;
    // }

    // Testing
    this.disableSwitching = true;

  }
}
