import { UserType } from '../enums/UserType-enum.model';
import { College } from './College.model';
import { Company } from './Company.model';
import { Job } from './Job.model';
import { UserName } from './UserName.model';

export interface User {
  userAccountId?: number;
  userName: UserName;
  mobileNumber: string;
  email: string;
  //this ispublic info
  password?: string;
  userType: UserType;
  userCompany?: Company;
  userCollege?: College;
  adminApprovalStatus: boolean;
  enabled: boolean;
  collegeUserJobPost: Job[];
  createdDate: number;
}