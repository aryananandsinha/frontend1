export interface Qualification {
  qualificationId?: number;
  qualificationName: string;
  qualificationSpecialisation?: string;
}
