export interface Location {
  locationId?: number;
  locationCity: string;
  locationDistrict: string;
  locationState: string;
  locationPincode: string;
  locationCountry: string;
}
