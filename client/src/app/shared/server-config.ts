export const ServerConfig = {
  serverBaseUrl: 'http://localhost:3000/',

  //Auth
  loginAuthEndpoint: 'api/auth/login',
  logoutAuthEndpoint: 'api/auth/logout',
  registerAuthEndpoint: 'api/auth/register',

  //College Admin or Company Admin
  collegeUsersAdminEndpoint: 'api/admin/college',
  companyUsersAdminEndpoint: 'api/admin/company',
  approveCompanySpocAdminEndpoint: 'api/admin/company/approval',
  approveCollegeSpocAdminEndpoint: 'api/admin/college/approval',

  //College Spoc and Company Spoc apply
  applyCollegeEndpoint: 'api/apply/company',
  applyCompanyEndpoint: 'api/apply/college',

  userEndpoint: 'apiusers',

  collegeEndpoint: 'api/colleges',

  companyEndpoint: 'api/companies',

  //jobposts
  jobPostEndpoint: 'api/jobposts',
  jobPostByCompanyEndpoint: 'api/jobposts/company',
  jobPostByCollegeUserEndpoint: 'api/jobposts/college/user',

  locationEndpoint: 'api/locations',

  skillsetEndpoint: 'api/skillsets',

  qualificationEndpoint: 'api/qualifications',
};
